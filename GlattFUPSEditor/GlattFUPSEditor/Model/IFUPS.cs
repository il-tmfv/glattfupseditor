﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlattFUPSEditor.Model
{
    public class FUPSText
    {
        public string Text { get; set; }
    }

    public class FUPS
    {
        public string Step { get; set; }
        public string Index { get; set; }
        public BindingList<FUPSText> Texts { get; set; }
    }
}
