﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlattFUPSEditor.Model
{
    public class ObjectClass
    {
        public string ObjName { get; set; }
        public string ObjLabel { get; set; }
    }
}
