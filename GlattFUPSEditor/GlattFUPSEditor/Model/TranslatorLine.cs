﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlattFUPSEditor.Model
{
    public class TranslatorLine
    {
        public TranslatorLine(string line)
        {
            OriginalString = line;
            elements = line.Split(';');
            EnglishText = elements[engIndex];
            RussianText = elements[rusIndex];
            PropertyName = elements[propIndex];
        }

        public void CopyRusToEng()
        {
            EnglishText = RussianText;
        }

        private const int engIndex = 2;
        private const int rusIndex = 8;
        private const int propIndex = 1;

        public string GetStringForSave()
        {
            elements[engIndex] = EnglishText;
            elements[rusIndex] = RussianText;
            return string.Join(";", elements);
        }

        private string[] elements;
        public string OriginalString { get; set; }
        public string EnglishText { get; set; }
        public string RussianText { get; set; }
        public string PropertyName { get; set; }
    }
}
