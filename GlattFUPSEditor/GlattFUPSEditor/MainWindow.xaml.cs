﻿using System.Windows;
using GlattFUPSEditor.ViewModel;

namespace GlattFUPSEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Closing += (s, e) => ViewModelLocator.Cleanup();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Editor_Click(object sender, RoutedEventArgs e)
        {
            ClipBoardWindow w = new ClipBoardWindow();
            w.Show();
        }

        private void PicTranslator_Click(object sender, RoutedEventArgs e)
        {
            TranslatorWindow w = new TranslatorWindow();
            w.Show();
        }

    }
}