﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GlattFUPSEditor.Model;
using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows;

namespace GlattFUPSEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ClipBoardViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the ClipBoardViewModel class.
        /// </summary>
        public ClipBoardViewModel()
        {
            PPAs = new BindingList<ObjectClass>();
            PPBs = new BindingList<ObjectClass>();
            Actions = new BindingList<ObjectClass>();
        }

        private void CopyToClipBoard(object obj)
        {
            var oc = obj as ObjectClass;
            Clipboard.Clear();
            Clipboard.SetText(oc.ObjLabel);
        }

        private void ReadFromClipBoard(BindingList<ObjectClass> list)
        {
            string cbText = "";
            if (Clipboard.ContainsText())
                cbText = Clipboard.GetText();
            else
                return;

            if (list == null)
                throw new ArgumentNullException("list", "list == null");

            list.Clear();

            var lines = cbText.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
 
            foreach (var line in lines)
            {
                if (!String.IsNullOrEmpty(line))
                {
                    string[] tmp = line.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                    list.Add(new ObjectClass() { ObjName = tmp[0], ObjLabel = tmp[tmp.Length - 1] });
                }
            }
        }

        private RelayCommand _readAction;
        /// <summary>
        /// Gets the ReadAction.
        /// </summary>
        public RelayCommand ReadAction
        {
            get
            {
                return _readAction
                    ?? (_readAction = new RelayCommand(
                    () =>
                    {
                        ReadFromClipBoard(Actions);
                    }));
            }
        }

        private RelayCommand _readPPB;
        /// <summary>
        /// Gets the ReadPPB.
        /// </summary>
        public RelayCommand ReadPPB
        {
            get
            {
                return _readPPB
                    ?? (_readPPB = new RelayCommand(
                    () =>
                    {
                        ReadFromClipBoard(PPBs);
                    }));
            }
        }

        private RelayCommand _readPPA;
        /// <summary>
        /// Gets the ReadPPA.
        /// </summary>
        public RelayCommand ReadPPA
        {
            get
            {
                return _readPPA
                    ?? (_readPPA = new RelayCommand(
                    () =>
                    {
                        ReadFromClipBoard(PPAs);
                    }));
            }
        }

        private RelayCommand<object> _copyPPB;
        /// <summary>
        /// Gets the CopyPPB.
        /// </summary>
        public RelayCommand<object> CopyPPB
        {
            get
            {
                return _copyPPB
                    ?? (_copyPPB = new RelayCommand<object>(ExecuteCopyPPB));
            }
        }

        private void ExecuteCopyPPB(object parameter)
        {
            CopyToClipBoard(parameter);
        }

        private RelayCommand<object> _copyPPA;
        /// <summary>
        /// Gets the CopyPPA.
        /// </summary>
        public RelayCommand<object> CopyPPA
        {
            get
            {
                return _copyPPA
                    ?? (_copyPPA = new RelayCommand<object>(ExecuteCopyPPA));
            }
        }

        private void ExecuteCopyPPA(object parameter)
        {
            CopyToClipBoard(parameter);
        }

        private RelayCommand<object> _copyAction;
        /// <summary>
        /// Gets the CopyAction.
        /// </summary>
        public RelayCommand<object> CopyAction
        {
            get
            {
                return _copyAction
                    ?? (_copyAction = new RelayCommand<object>(ExecuteCopyAction));
            }
        }

        private void ExecuteCopyAction(object parameter)
        {
            CopyToClipBoard(parameter);
        }

        /// <summary>
        /// The <see cref="Actions" /> property's name.
        /// </summary>
        public const string ActionsPropertyName = "Actions";
        private BindingList<ObjectClass> _actions = null;
        /// <summary>
        /// Sets and gets the Actions property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public BindingList<ObjectClass> Actions
        {
            get
            {
                return _actions;
            }

            set
            {
                if (_actions == value)
                {
                    return;
                }

                _actions = value;
                RaisePropertyChanged(ActionsPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="PPAs" /> property's name.
        /// </summary>
        public const string PPAsPropertyName = "PPAs";
        private BindingList<ObjectClass> _ppas = null;
        /// <summary>
        /// Sets and gets the PPAs property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public BindingList<ObjectClass> PPAs
        {
            get
            {
                return _ppas;
            }

            set
            {
                if (_ppas == value)
                {
                    return;
                }

                _ppas = value;
                RaisePropertyChanged(PPAsPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="PPBs" /> property's name.
        /// </summary>
        public const string PPBsPropertyName = "PPBs";
        private BindingList<ObjectClass> _ppbs = null;
        /// <summary>
        /// Sets and gets the PPBs property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public BindingList<ObjectClass> PPBs
        {
            get
            {
                return _ppbs;
            }

            set
            {
                if (_ppbs == value)
                {
                    return;
                }

                _ppbs = value;
                RaisePropertyChanged(PPBsPropertyName);
            }
        }

    }
}