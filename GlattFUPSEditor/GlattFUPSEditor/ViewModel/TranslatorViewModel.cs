﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GlattFUPSEditor.Model;
using Microsoft.Win32;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace GlattFUPSEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class TranslatorViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the TranslatorViewModel class.
        /// </summary>
        public TranslatorViewModel()
        {
            TranslatorLines = new BindingList<TranslatorLine>();
        }

        /// <summary>
        /// The <see cref="PicName" /> property's name.
        /// </summary>
        public const string PicNamePropertyName = "PicName";
        private string _picName = "*";
        /// <summary>
        /// Sets and gets the PicName property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string PicName
        {
            get
            {
                return _picName;
            }

            set
            {
                if (_picName == value)
                {
                    return;
                }

                _picName = value;
                RaisePropertyChanged(PicNamePropertyName);
            }
        }

        /// <summary>
        /// The <see cref="FileHeader" /> property's name.
        /// </summary>
        public const string FileHeaderPropertyName = "FileHeader";
        private string _fileHeader = "*";
        /// <summary>
        /// Sets and gets the FileHeader property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string FileHeader
        {
            get
            {
                return _fileHeader;
            }

            set
            {
                if (_fileHeader == value)
                {
                    return;
                }

                _fileHeader = value;
                RaisePropertyChanged(FileHeaderPropertyName);
            }
        }

        /// <summary>
        /// The <see cref="FileName" /> property's name.
        /// </summary>
        public const string FileNamePropertyName = "FileName";
        private string _fileName = "***";
        /// <summary>
        /// Sets and gets the FileName property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string FileName
        {
            get
            {
                return _fileName;
            }

            set
            {
                if (_fileName == value)
                {
                    return;
                }

                _fileName = value;
                //SaveFileCommand.RaiseCanExecuteChanged();
                //ReadAgainCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged(FileNamePropertyName);
            }
        }

        /// <summary>
        /// The <see cref="TranslatorLines" /> property's name.
        /// </summary>
        public const string LinesPropertyName = "TranslatorLines";
        private BindingList<TranslatorLine> _lines = null;
        /// <summary>
        /// Sets and gets the Lines property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public BindingList<TranslatorLine> TranslatorLines
        {
            get
            {
                return _lines;
            }

            set
            {
                if (_lines == value)
                {
                    return;
                }

                _lines = value;
                RaisePropertyChanged(LinesPropertyName);
            }
        }

        private void ReadFile()
        {
            TranslatorLines.Clear();
            using (StreamReader sr = new StreamReader(FileName))
            {
                if (!sr.EndOfStream)
                    PicName = sr.ReadLine();

                if (!sr.EndOfStream)
                    FileHeader = sr.ReadLine();

                string str = "";

                while (!sr.EndOfStream)
                {
                    str = sr.ReadLine();
                    TranslatorLines.Add(new TranslatorLine(str));
                }
                
                // delete line where rus == eng
                TranslatorLines = new BindingList<TranslatorLine>(TranslatorLines.Where(l => l.RussianText != l.EnglishText).ToList());
            }
        }

        private RelayCommand _readFileCommand;
        /// <summary>
        /// Gets the ReadFileCommand.
        /// </summary>
        public RelayCommand ReadFileCommand
        {
            get
            {
                return _readFileCommand
                    ?? (_readFileCommand = new RelayCommand(
                    () =>
                    {
                        OpenFileDialog openFileDialog = new OpenFileDialog();
                        if (openFileDialog.ShowDialog() == true)
                        {
                            FileName = openFileDialog.FileName;
                            ReadFile();
                        }
                    }));
            }
        }

        private RelayCommand<object> _copyRusToEng;
        /// <summary>
        /// Gets the CopyRusToEng.
        /// </summary>
        public RelayCommand<object> CopyRusToEng
        {
            get
            {
                return _copyRusToEng
                    ?? (_copyRusToEng = new RelayCommand<object>(ExecuteCopyRusToEng));
            }
        }

        private void ExecuteCopyRusToEng(object parameter)
        {
            var line = parameter as TranslatorLine;
            if (line != null)
            {
                int index = TranslatorLines.IndexOf(line);
                line.CopyRusToEng();

                BindingList<TranslatorLine> buf = new BindingList<TranslatorLine>(TranslatorLines.Take(index).ToList());
                buf.Add(line);
                buf = new BindingList<TranslatorLine>(buf.Concat(TranslatorLines.Skip(index+1)).ToList());

                TranslatorLines = buf;
            }
        }

        private void SaveFile()
        {
            using(StreamWriter sw = new StreamWriter(FileName + "!", false))
            {
                sw.WriteLine(PicName);
                sw.WriteLine(FileHeader);
                foreach (var line in TranslatorLines)
                {
                    sw.WriteLine(line.GetStringForSave());
                }
            }
        }

        private RelayCommand _saveFileCommand;
        /// <summary>
        /// Gets the SaveFileCommand.
        /// </summary>
        public RelayCommand SaveFileCommand
        {
            get
            {
                return _saveFileCommand
                    ?? (_saveFileCommand = new RelayCommand(
                    () =>
                    {
                        SaveFile();
                    }));
            }
        }
    }
}