﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GlattFUPSEditor.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace GlattFUPSEditor.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            //test
            TreeItems = new BindingList<FUPS>();
        }

        /// <summary>
        /// The <see cref="TreeItems" /> property's name.
        /// </summary>
        public const string TreeItemsPropertyName = "TreeItems";
        private BindingList<FUPS> _treeItems = null;

        /// <summary>
        /// Sets and gets the TreeItems property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public BindingList<FUPS> TreeItems
        {
            get
            {
                return _treeItems;
            }

            set
            {
                if (_treeItems == value)
                {
                    return;
                }

                _treeItems = value;
                RaisePropertyChanged(TreeItemsPropertyName);
            }
        }


        private RelayCommand _addNodeCommand;
        /// <summary>
        /// Gets the AddNodeCommand.
        /// </summary>
        public RelayCommand AddNodeCommand
        {
            get
            {
                return _addNodeCommand
                    ?? (_addNodeCommand = new RelayCommand(
                    () =>
                    {
                        if (!AddNodeCommand.CanExecute(null))
                        {
                            return;
                        }

                        BindingList<FUPSText> bl = new BindingList<FUPSText>();
                        for (int i = 0; i < 10; i++)
                            bl.Add(new FUPSText{ Text = "" });
                        TreeItems.Add(new FUPS { Step = "*", Index = "0", Texts = bl });
                    },
                    () => true));
            }
        }

        private void ReadFromFile()
        {
            using(StreamReader sr = new StreamReader(FileName, Encoding.GetEncoding(1251)))
            {
                string row = "";
                string[] rowEelemnts;
                TreeItems.Clear();
                FUPS fups;

                while (!sr.EndOfStream)
                {
                    row = sr.ReadLine();
                    rowEelemnts = row.Split(',').Select(x => x.Trim()).ToArray();
                    fups = new FUPS { Step = rowEelemnts[0], 
                                      Index = rowEelemnts[1], 
                                      Texts = new BindingList<FUPSText>(rowEelemnts.Skip(2).Select(x => new FUPSText { Text = x } ).ToList()) };
                    TreeItems.Add(fups);
                }
            }
        }

        private void SaveToFile()
        {
            string row = "";
            string header = "";

            //удаление отмеченных позиций
            TreeItems = new BindingList<FUPS>(TreeItems.Where(x => x.Step != "*").ToList());

            //сортировка по шагу
            TreeItems = new BindingList<FUPS>(TreeItems.OrderBy(x => Int32.Parse(x.Step)).ToList());
            
            using (StreamWriter sw = new StreamWriter(FileName, false, Encoding.GetEncoding(1251)))
            {
                foreach (var item in TreeItems)
                {
                    header = item.Step + "," + item.Index + ",";
                    row = string.Join(",", item.Texts.Select(x => x.Text.Replace(",", ".").Trim()));
                    sw.WriteLine(header + row);
                }
            }
        }

        private RelayCommand _saveComamnd;
        /// <summary>
        /// Gets the SaveFileCommand.
        /// </summary>
        public RelayCommand SaveFileCommand
        {
            get
            {
                return _saveComamnd
                    ?? (_saveComamnd = new RelayCommand(
                    () =>
                    {
                        if (!SaveFileCommand.CanExecute(null))
                        {
                            return;
                        }
                        SaveToFile();
                    },
                    () => FileName != "***"));
            }
        }

        /// <summary>
        /// The <see cref="FileName" /> property's name.
        /// </summary>
        public const string FileNamePropertyName = "FileName";
        private string _fileName = "***";
        /// <summary>
        /// Sets and gets the FileName property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public string FileName
        {
            get
            {
                return _fileName;
            }

            set
            {
                if (_fileName == value)
                {
                    return;
                }

                _fileName = value;
                SaveFileCommand.RaiseCanExecuteChanged();
                ReadAgainCommand.RaiseCanExecuteChanged();
                RaisePropertyChanged(FileNamePropertyName);
            }
        }

        private RelayCommand _readAgainCommand;

        /// <summary>
        /// Gets the ReadAgainCommand.
        /// </summary>
        public RelayCommand ReadAgainCommand
        {
            get
            {
                return _readAgainCommand
                    ?? (_readAgainCommand = new RelayCommand(
                    () =>
                    {
                        if (!ReadAgainCommand.CanExecute(null))
                        {
                            return;
                        }

                        ReadFromFile();
                    },
                    () => FileName != "***"));
            }
        }

        private RelayCommand _openFileCommand;
        /// <summary>
        /// Gets the OpenFileCommand.
        /// </summary>
        public RelayCommand OpenFileCommand
        {
            get
            {
                return _openFileCommand
                    ?? (_openFileCommand = new RelayCommand(
                    () =>
                    {
                        OpenFileDialog openFileDialog = new OpenFileDialog();
                        if (openFileDialog.ShowDialog() == true)
                        {
                            FileName = openFileDialog.FileName;
                            ReadFromFile();
                        }
                    }));
            }
        }

        ////public override void Cleanup()
        ////{
        ////    // Clean up if needed

        ////    base.Cleanup();
        ////}
    }
}